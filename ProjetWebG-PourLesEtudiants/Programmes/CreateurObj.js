/**
 * @author Mathilde
 */

import {
    MeshPhongMaterial, 
    Mesh,
    PlaneGeometry,
	Math,
    CanvasTexture,
    BoxGeometry,
    MeshPhysicalMaterial,
    FrontSide
} from "../Libs/three.module.js";

var CreateurObj = function ( scene ) {
    this.scene = scene;

    this.CreatePanneau = function(nameObj, content, couleurContent, tailleContent, couleurFond, position, rotation){
        var geometry = new PlaneGeometry( 5, 3 );
        console.log("Paramètre PlaneGeometry", geometry.parameters);

        var canvas = document.createElement('canvas');
        console.log("Paramètres canvas : width => ", canvas.width);
        console.log("Paramètres canvas : height => ", canvas.height);

        let context = canvas.getContext('2d');

        context.fillStyle = couleurFond;
        context.fillRect(0, 0, canvas.width, canvas.height);
        
        context.font = 'bold ' + tailleContent + 'px Arial';
        context.fillStyle = couleurContent;
        context.textAlign = 'center';
        context.textBaseline = "middle";

        //context.fillText(content, 150, 15, canvas.width);
        this.addMultiLineText(content, 150, tailleContent, tailleContent, 150, context);

        var text = new CanvasTexture(canvas);

        var material = new MeshPhongMaterial( {color: 0xffff00, map: text, side: FrontSide} );
        var plane = new Mesh( geometry, material );
        plane.name = nameObj;
        console.log(plane.name, " : id => ", plane.id);

        plane.position.x = position.x;
        plane.position.y = position.y;
        plane.position.z = position.z;
        
        plane.rotation.x = Math.degToRad(rotation.x);
        plane.rotation.y = Math.degToRad(rotation.y);
        plane.rotation.z = Math.degToRad(rotation.z);

        this.scene.add( plane );
    }

    this.addMultiLineText = function(text, x, y, lineHeight, fitWidth, oContext, bDebug) { 
        var sections = text.split("\n"); 
        console.log(sections);
        console.log("Section length : ", sections.length);

        for (var i = 0; i < sections.length; i++)
        { 
            oContext.fillText(sections[i], x, y+lineHeight*i);
        }
    };

    this.CreateBox = function(nom, dim, position, couleur){
        var params = {
        color: couleur,
        transparency: 0.70,
        envMapIntensity: 1,
        lightIntensity: 1,
        exposure: 1
        };

        var geometry = new BoxGeometry( dim, dim, dim );

        var material = new MeshPhysicalMaterial( {
            color: params.color,
            metalness: 0,
            roughness: 0,
            alphaTest: 0.5,
            envMapIntensity: params.envMapIntensity,
            depthWrite: false,
            transparency: params.transparency, // use material.transparency for glass materials
            opacity: 1,                        // set material.opacity to 1 when material.transparency is non-zero
            transparent: true
        } );

        var mesh = new Mesh(geometry, material);
        mesh.name = nom;
        console.log(mesh.name, " : id => ", mesh.id);
        mesh.position.x = position.x;
        mesh.position.y = position.y;
        mesh.position.z = position.z;
        mesh.visible = false;

        this.scene.add(mesh);
    }
};

export { CreateurObj };